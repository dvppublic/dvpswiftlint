# DVPSwiftLint

Demo project to demonstrate the power of SwiftLint. (reference: https://github.com/realm/SwiftLint )

    1. Add pre-commit hook.
client side .git/hooks pre-commit script: https://gist.github.com/haruair/0b7607a988990a36d018190fe82dd5fb
Should use `git init` if the repo does not have `.git/hooks` folder. Afterwards add the SwiftLint pre-commit script and rename `pre-commit.sample` to `pre-commit`.

    2. Install SwiftLint and add Run Script.
The following guide will help you with installing `SwiftLint` (Step 1 & Step 2) and adding `Run Script` to project's target that checks if `SwiftLint` (Step 3) is installed: https://iphoneappcode.blogspot.co.uk/2017/03/how-to-integrate-swiftlint-library-into.html
`
if which swiftlint >/dev/null; then
    swiftlint
else
    echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint , More info at: https://iphoneappcode.blogspot.co.uk/2017/03/how-to-integrate-swiftlint-library-into.html"
    exit 1
fi`

    3. Clone project from remote repository.
On cloning remote repo, hooks are not shared automatically. And most likely the `.git/hooks` will not even exist in the `.git` direcotry. Fair not, poitn 4 is explaining how to get project's hooks. (If you want to see all sample hooks in repository, you can run `git init` - this will automatically generate `.git/hooks` folder with all scripts' samples) ; Usefull links: 1) https://www.darrenlester.com/blog/including-hooks-in-a-git-repository ; 2) https://stackoverflow.com/a/37861972/3883492

    4. Config hooks directory
Use `git config core.hooksPath hooks`  in the main project's directory. This replaces the `.git/hooks` directory with just `hooks` directory. Note after this config is running, any scripts in `.git/hooks` will not be executed. Only valid scripts in `hooks` directory will be looked up by git. (usable in git version >= 2.9, symlinks could be used for earlier git versions)

    5. Add XCode Source Extension > https://github.com/norio-nomura/SwiftLintForXcode <
Use this XCode extension for GUI autocorrect command. Editor > SwiftLint > Run swiftlint autocorrect
    
    Additional Links:
        - https://stackoverflow.com/a/11197286/3883492 - how to show hidden files on MacOS in finder;
        - What is SwiftLint: https://academy.realm.io/posts/slug-jp-simard-swiftlint/
        - https://github.com/realm/SwiftLint/blob/master/Rules.md - All SwiftLint Rules + examples of triggering and not triggering code;
        - https://github.com/realm/SwiftLint/issues/413 - discussion about SwiftLint on pre-commit hook and running only on staged files. (https://github.com/realm/SwiftLint/issues/413#issuecomment-174581706 ; https://github.com/realm/SwiftLint/issues/413#issuecomment-184077062)
        - https://github.com/realm/SwiftLint#configuration - sample configuration of the .swiftlint.yml file.
        - https://github.com/theswiftdev/awesome-xcode-extensions - notable XCode extension related to SwiftLint is: https://github.com/norio-nomura/SwiftLintForXcode
        - Remove SwiftLint https://stackoverflow.com/questions/37393203/remove-swiftlint-installed-from-package
        
    Commands
- swiftlint help
- swiftlint autocorrect // swiftlint autocorrect --path MySwiftFileName.swift
